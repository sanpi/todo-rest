<?php

namespace Todo\ApiBundle\Request\ParamConverter;

use Todo\ApiBundle\Service\Tasks;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;

class TaskParamConverter implements ParamConverterInterface
{
    private $tasks;

    public function __construct(Tasks $tasks)
    {
        $this->tasks = $tasks;
    }

    public function supports(ParamConverter $configuration)
    {
        $class = $configuration->getClass();

        return (
            $class === 'Todo\Task\Simple'
            || $class === 'Todo\Task\Advanced'
        );
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        $class = $configuration->getClass();
        $type = 'simple';
        if ($class === 'Todo\Task\Advanced') {
            $type = 'advanced';
        }

        $id = $request->get('task');
        $tasks = $this->tasks->getTasks($request->attributes->get('username'), $type);

        if (!isset($tasks[$id])) {
            throw new NotFoundHttpException("Task $id not found");
        }

        $request->attributes->set('task', $tasks[$id]);
    }

}
