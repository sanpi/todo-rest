<?php

namespace Todo\ApiBundle\Service;

use Todo\ApiBundle\Storage\Storage;

class Tasks
{
    private $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    public function getTasks($username, $type)
    {
        return $this->getCollection($username, $type)
            ->getTasks();
    }

    public function getCollection($username, $type)
    {
        $contents = $this->storage->getFileContent($username, 'todo.txt');
        return new \Todo\Collection($contents, $type);
    }

    public function save($username, $collection)
    {
        $this->storage->setFileContent($username, 'todo.txt', (string)$collection);
    }
}
