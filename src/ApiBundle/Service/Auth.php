<?php

namespace Todo\ApiBundle\Service;

use Sanpi\Http\Auth\Auth as HttpAuth;

class Auth
{
    private $auth;
    private $passwdFile;
    private $dataDirectory;

    public function __construct($dataDirectory, HttpAuth $auth)
    {
        $this->dataDirectory = $dataDirectory;
        $this->passwdFile = "$dataDirectory/htpasswd";
        $this->auth = $auth;
    }

    public function __call($name, $args)
    {
        return call_user_func_array([$this->auth, $name], $args);
    }

    public function getUsers()
    {
        $content = file_get_contents($this->passwdFile);
        return (array)json_decode($content);
    }

    public function createUser($username, $password, $email)
    {
        $users = $this->getUsers();

        if (!isset($users[$username])) {
            $users[$username] = compact('password', 'email');
            $this->saveUsers($users);

            mkdir("{$this->dataDirectory}/$username");
            touch("{$this->dataDirectory}/$username/todo.txt");
        }
        else {
            throw new \RuntimeException("User '$username' already exist");
        }
    }

    public function saveUsers($users)
    {
        $content = json_encode($users);
        file_put_contents($this->passwdFile, $content);
    }
}
