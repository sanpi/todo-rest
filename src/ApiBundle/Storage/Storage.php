<?php

namespace Todo\ApiBundle\Storage;

interface Storage
{
    public function getFileContent($username, $filename);
    public function setFileContent($username, $filename, $contents);
}
