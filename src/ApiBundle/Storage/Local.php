<?php

namespace Todo\ApiBundle\Storage;

class Local implements Storage
{
    private $baseDirectory;

    public function __construct($baseDirectory)
    {
        $this->baseDirectory = $baseDirectory;
    }

    public function getFileContent($username, $filename)
    {
        return file_get_contents(
            $this->getFilename($username, $filename)
        );
    }

    public function setFileContent($username, $filename, $contents)
    {
        file_put_contents(
            $this->getFilename($username, $filename),
            $contents
        );
    }

    private function getFilename($username, $filename)
    {
        return "{$this->baseDirectory}/$username/todo.txt";
    }
}
