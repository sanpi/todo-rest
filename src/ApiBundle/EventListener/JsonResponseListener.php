<?php

namespace Todo\ApiBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class JsonResponseListener
{
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $headers = $event->getResponse()->headers;
        $headers->set('Access-Control-Allow-Origin', '*');
    }
}
