<?php

namespace Todo\ApiBundle\EventListener;

use Todo\ApiBundle\Service\Auth;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AuthRequestListener
{
    private $auth;
    private $realm = 'Restricted area';

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if (!$event->isMasterRequest()) {
            return;
        }

        $method = $request->server->get('REQUEST_METHOD');
        if ($method === 'OPTIONS') {
            return;
        }

        if ($method === 'POST' && $request->server->get('REQUEST_URI') === '/users') {
            return;
        }

        if ($this->auth->hasAuthorization($request->headers)) {
            $authentified = false;
            $users = $this->auth->getUsers();

            foreach ($users as $username => $user) {
                $authentified = $this->auth->authenticate(
                    $method, $request->headers,
                    $username, $user->password
                );
                if ($authentified === true) {
                    $request->attributes->set('username', $username);
                    break;
                }
            }

            if ($authentified === false) {
                throw new AccessDeniedHttpException();
            }
        }
        else {
            $challenge = $this->auth->getChallenge($this->realm, 'auth');
            throw new UnauthorizedHttpException($challenge);
        }
    }
}
