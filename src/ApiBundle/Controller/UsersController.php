<?php

namespace Todo\ApiBundle\Controller;

use FOS\RestBundle\View\View;
use Todo\ApiBundle\Service\Auth;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UsersController
{
    private $auth;

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    public function optionsUsersAction()
    {
        return new View('', 204, [
            'Access-Control-Allow-Headers' => 'Content-Type',
            'Access-Control-Allow-Methods' => 'OPTIONS, POST',
        ]);
    }

    public function postUsersAction(Request $request)
    {
        foreach (['username', 'password', 'email'] as $param) {
            $$param = $request->request->get($param);
            if ($$param === null) {
                throw new BadRequestHttpException();
            }
        }

        try {
            $this->auth->createUser($username, $password, $email);
        }
        catch (\RuntimeException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        return new View([
            'status' => 'success',
            'status_code' => 200,
            'status_text' => 'OK',
            'current_content' => '',
            'message' => "User $username created",
        ]);
    }
}
