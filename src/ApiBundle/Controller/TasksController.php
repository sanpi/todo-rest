<?php

namespace Todo\ApiBundle\Controller;

use FOS\RestBundle\View\View;
use Todo\ApiBundle\Service\Tasks;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TasksController
{
    private $tasks;

    public function __construct(Tasks $tasks)
    {
        $this->tasks = $tasks;
    }

    public function optionsTasksAction()
    {
        return new View('', 204, [
            'Access-Control-Allow-Headers' => 'Content-Type, Authorization',
            'Access-Control-Allow-Methods' => 'GET, OPTIONS, POST',
        ]);
    }

    public function getTasksAction(Request $request)
    {
        $tasks = $this->tasks->getTasks(
            $request->attributes->get('username'),
            'simple'
        );
        return new View($tasks, 200);
    }

    public function optionsTaskAction($id)
    {
        return new View('', 204, [
            'Access-Control-Allow-Headers' => 'Content-Type, Authorization',
            'Access-Control-Allow-Methods' => 'DELETE, GET, OPTIONS, PATCH, POST, PUT',
        ]);
    }

    public function getTaskAction(\Todo\Task\Simple $task)
    {
        return new View($task, 200);
    }

    public function postTasksAction(Request $request)
    {
        $task = new \Todo\Task\Simple($request->get('raw'));
        if (empty($task->created)) {
            $task->created = date('Y-m-d');
        }

        $collection = $this->getCollection($request);
        $collection[] = $task;
        $this->tasks->save($request->attributes->get('username'), $collection);

        return new View([
            'status' =>'success',
            'status_code' => 201,
            'status_text' => 'created',
            'current_content' => '',
            'message' => "Task {$task->id} created",
            'id' => $task->id,
        ], 201);
    }

    public function deleteTaskAction(\Todo\Task\Simple $task, Request $request)
    {
        $collection = $this->getCollection($request);
        unset($collection[$task->id]);
        $this->tasks->save($request->attributes->get('username'), $collection);

        return new View([
            'status' => 'success',
            'status_code' => 200,
            'status_text' => 'OK',
            'current_content' => '',
            'message' => 'Task deleted',
        ]);
    }

    public function patchTaskAction(\Todo\Task\Simple $task, Request $request)
    {
        foreach ($request->request->all() as $name => $value) {
            if (property_exists($task, $name)) {
                if ($value === "false") {
                    $value = false;
                }
                $task->$name = $value;
            }
            else {
                throw new BadRequestHttpException("Unknow property '$name'");
            }
        }

        $collection = $this->getCollection($request);
        $collection[$task->id] = $task;
        $this->tasks->save($request->attributes->get('username'), $collection);

        return new View([
            'status' => 'success',
            'status_code' => 200,
            'status_text' => 'OK',
            'current_content' => '',
            'message' => "Task {$task->id} updated",
        ]);
    }

    public function putTaskAction(\Todo\Task\Simple $task, Request $request)
    {
        $id = $task->id;
        $task = new \Todo\Task\Simple($request->get('raw'));
        $collection = $this->getCollection($request);
        $collection[$id] = $task;
        $this->tasks->save($request->attributes->get('username'), $collection);

        return new View([
            'status' => 'success',
            'status_code' => 200,
            'status_text' => 'OK',
            'current_content' => '',
            'message' => "Task {$task->id} updated",
        ]);
    }

    public function optionsTaskCompleteAction($id)
    {
        return new View('', 204, [
            'Access-Control-Allow-Headers' => 'Content-Type, Authorization',
            'Access-Control-Allow-Methods' => 'POST',
        ]);
    }

    public function postTaskCompleteAction(\Todo\Task\Simple $task, Request $request)
    {
        $request->request->set('complete', true);
        $request->request->set('completed', date('Y-m-d'));

        return $this->patchTaskAction($task, $request);
    }

    public function optionsTaskUncompleteAction($id)
    {
        return new View('', 204, [
            'Access-Control-Allow-Headers' => 'Content-Type, Authorization',
            'Access-Control-Allow-Methods' => 'POST',
        ]);
    }

    public function postTaskUncompleteAction(\Todo\Task\Simple $task, Request $request)
    {
        $request->request->set('complete', false);
        $request->request->set('completed', null);

        return $this->patchTaskAction($task, $request);
    }

    private function getCollection(Request $request)
    {
        return $this->tasks->getCollection(
            $request->attributes->get('username'),
            'simple'
        );
    }
}
