<?php

namespace Todo\ApiBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $treeBuilder->root('todo_api')
            ->children()
                ->scalarNode('data_dir')
                    ->cannotBeEmpty()
                    ->defaultValue('%kernel.root_dir%/data')
                ->end()
            ->end();

        return $treeBuilder;
    }
}
