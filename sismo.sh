#!/bin/bash

wget http://getcomposer.org/installer -O - | php;

cp app/config/parameters.yml{.dist,}
cp behat.yml{-dist,}

./composer.phar install --dev;

php -S localhost:8080 -t web web/app.php &
pid=$!

./bin/behat -fprogress

kill -9 $pid
