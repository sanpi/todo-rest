# todo.rest

[![Build Status](https://ci.homecomputing.fr/todo-rest/build/status)](https://ci.homecomputing.fr/todo-rest)

A REST API for todo.txt file.

## Installation

    $ git clone git@github.com:sanpii/todo.rest.git
    $ cd todo.rest
    $ curl http://getcomposer.org/installer | php
    $ ./composer.phar install

For development purpose only:

    $ cp src/config/{development,current}.php
    $ php -S localhost:8080 -t web/ web/app_dev.php

## Front end

REST API is cool, but hard to use without a front-end.

* [todo.webapp](https://github.com/sanpii/todo.webapp)
