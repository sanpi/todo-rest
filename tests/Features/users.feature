Feature: User

    Background:
        Given I add "Accept" header equal to "application/json"
        And I create the file "app/data/htpasswd" contening:
        """
        """

    Scenario: Task access control
        When I send a POST request to "/users"
        Then the header "Access-Control-Allow-Origin" should be equal to "*"

    Scenario: Tasks allowed method
        When I send a OPTIONS request to "/users"
        Then the response status code should be 204
        And the header "Access-Control-Allow-Methods" should be equal to "OPTIONS, POST"
        And the header "Access-Control-Allow-Headers" should be equal to "Content-Type"

    Scenario: Invalid request
        When I send a POST request to "/users"
        Then the response status code should be 400

    Scenario: Registration
        When I send a POST request to "/users" with parameters:
            | key      | value    |
            | username | foo      |
            | password | bor      |
            | email    | foo2@bor |
        Then the response status code should be 200
        And the response should be in JSON
        And the JSON should be equal to:
        """
        {
            "status": "success",
            "status_code": 200,
            "status_text": "OK",
            "current_content": "",
            "message": "User foo created"
        }
        """

        When I send a POST request to "/users" with parameters:
            | key      | value   |
            | username | foo     |
            | password | bor     |
            | email    | foo@bor |
        Then the response status code should be 400
        And the response should be in JSON
        And the JSON should be equal to:
        """
        {
            "status": "error",
            "status_code": 400,
            "status_text": "Bad Request",
            "current_content": "",
            "message": "User 'foo' already exist"
        }
        """
