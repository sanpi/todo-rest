Feature: Authentication
    via http digest authentication
    http://tools.ietf.org/html/rfc2617

    Background:
        Given I create the directory "app/data/foo/"
        And I create the file "app/data/htpasswd" contening:
        """
        {"foo":{"password":"bar"}}
        """
        And I add "Accept" header equal to "application/json"

    Scenario: Unauthentificate
        When I send a GET request to "/tasks"
        Then the response status code should be 401
        And the response should be in JSON
        And the JSON should be equal to:
        """
        {
            "status": "error",
            "status_code": 401,
            "status_text": "Unauthorized",
            "current_content": "",
            "message": ""
        }
        """

    Scenario: Invalid authentication
        When I send a GET request to "/tasks"
        And I set digest Authentication with "foo" and "invalid"
        Then the response status code should be 403
        And the response should be in JSON
        And the JSON should be equal to:
        """
        {
            "status": "error",
            "status_code": 403,
            "status_text": "Forbidden",
            "current_content": "",
            "message": ""
        }
        """

    Scenario: Valid authentication
        Given I create the file "app/data/foo/todo.txt" contening:
        """
        """
        When I send a GET authenticate request to "/tasks" with foo:bar
        Then the response status code should be 200
        And the response should be in JSON
