Feature: REST API

    Background:
        Given I create the directory "app/data/foo/"
        And I create the file "app/data/foo/todo.txt" contening:
        """
        (A) Crack the Da Vinci Code.
        (B) +winning Win.
        @context Give it some context.
        Just a POD: Plain old task.
        (C) +project @context This one has it all!
        (C) 2012-02-03 This one has a date!
        (B) 2012-03-04 +project @context This one has a date and a context AND a project!
        x 2012-04-03 This one has no priority and a date.
        """
        And I create the file "app/data/htpasswd" contening:
        """
        {"foo":{"password":"bar"}}
        """
        And I add "Accept" header equal to "application/json"

    Scenario: Task access control
        When I send a GET authenticate request to "/tasks/0" with foo:bar
        Then the header "Access-Control-Allow-Origin" should be equal to "*"

    Scenario: Tasks allowed method
        When I send a OPTIONS request to "/tasks/0"
        Then the header "Access-Control-Allow-Methods" should be equal to "DELETE, GET, OPTIONS, PATCH, POST, PUT"
        And the header "Access-Control-Allow-Headers" should be equal to "Content-Type, Authorization"
        Then the response status code should be 204

    Scenario: Get one task
        When I send a GET authenticate request to "/tasks/0" with foo:bar
        Then the response status code should be 200
        And the response should be in JSON
        And the JSON should be equal to:
        """
        {
            "id": 0,
            "raw": "(A) Crack the Da Vinci Code.",
            "contexts": [],
            "projects": [],
            "priority": "A",
            "complete": false,
            "description": "Crack the Da Vinci Code."
        }
        """

    Scenario: Get unknow task
        When I send a GET authenticate request to "/tasks/100" with foo:bar
        Then the response status code should be 404
        And the response should be in JSON
        And the JSON should be equal to:
        """
        {
            "status": "error",
            "status_code": 404,
            "status_text": "Not Found",
            "current_content": "",
            "message": "Task 100 not found"
        }
        """

    Scenario: Create a new task
        When I send a POST request to "/tasks" with parameters:
            | key | value                 |
            | raw | 2013-12-25 A new task |
        And I set digest Authentication with "foo" and "bar"
        Then the response status code should be 201
        And the response should be in JSON
        And the JSON should be equal to:
        """
        {
            "status": "success",
            "status_code": 201,
            "status_text": "created",
            "current_content": "",
            "message": "Task 8 created",
            "id": 8
        }
        """

        When I send a GET authenticate request to "/tasks/8" with foo:bar
        Then the response should be in JSON
        And the JSON should be equal to:
        """
        {
            "id": 8,
            "raw": "2013-12-25 A new task",
            "created": "2013-12-25",
            "contexts": [],
            "projects": [],
            "complete": false,
            "description": "A new task"
        }
        """

    Scenario: Create a new task without created date
        The date should be automaticaly added

        Given I send a POST request to "/tasks" with parameters:
            | key | value      |
            | raw | A new task |
        And I set digest Authentication with "foo" and "bar"
        When I send a GET authenticate request to "/tasks/8" with foo:bar
        Then the JSON node "created" should exist

    Scenario: Update a task
        When I send a PATCH request to "/tasks/7" with parameters:
            | key      | value |
            | complete | false |
        And I set digest Authentication with "foo" and "bar"
        Then the response status code should be 200
        And the JSON should be equal to:
        """
        {
            "status": "success",
            "status_code": 200,
            "status_text": "OK",
            "current_content": "",
            "message": "Task 7 updated"
        }
        """

        When I send a GET authenticate request to "/tasks/7" with foo:bar
        Then the response should be in JSON
        And the JSON node "complete" should be equal to "0"

    Scenario: Delete a task
        When I send a DELETE authenticate request to "/tasks/7" with foo:bar
        Then the response status code should be 200
        And the JSON should be equal to:
        """
        {
            "status": "success",
            "status_code": 200,
            "status_text": "OK",
            "current_content": "",
            "message": "Task deleted"
        }
        """

        When I send a GET authenticate request to "/tasks/7" with foo:bar
        Then the response should be in JSON
        And the JSON should be equal to:
        """
        {
            "status": "error",
            "status_code": 404,
            "status_text": "Not Found",
            "current_content": "",
            "message": "Task 7 not found"
        }
        """

    Scenario: Update unknow task property
        When I send a PATCH request to "/tasks/0" with parameters:
            | key | value |
            | xxx | 1     |
        And I set digest Authentication with "foo" and "bar"
        Then the response status code should be 400
        """
        {
            "status": "error",
            "status_code": 400,
            "status_text": "Bad Request",
            "current_content": "",
            "message": "Unknow property 'xxx'"
        }
        """

    Scenario: Update unknow task property
        When I send a PATCH request to "/tasks/0" with parameters:
            | key | value |
            | xxx | 1     |
        Then the response status code should be 400
        """
        {
            "status": "error",
            "status_code": 400,
            "status_text": "Bad Request",
            "current_content": "",
            "message": "Unknow property 'xxx'"
        }
        """

    Scenario: Completly update a task
        When I send a PUT request to "/tasks/1" with parameters:
            | key | value               |
            | raw | An updated task     |
        And I set digest Authentication with "foo" and "bar"
        Then the response status code should be 200
        And the JSON should be equal to:
        """
        {
            "status": "success",
            "status_code": 200,
            "status_text": "OK",
            "current_content": "",
            "message": "Task 1 updated"
        }
        """

        When I send a GET authenticate request to "/tasks/1" with foo:bar
        Then the response should be in JSON
        And the JSON should be equal to:
        """
        {
            "id": 1,
            "raw": "An updated task",
            "contexts": [],
            "projects": [],
            "complete": false,
            "description": "An updated task"
        }
        """

    Scenario: Complete tasks allowed method
        When I send a OPTIONS request to "/tasks/0/complete"
        Then the header "Access-Control-Allow-Methods" should be equal to "POST"
        And the header "Access-Control-Allow-Headers" should be equal to "Content-Type, Authorization"
        Then the response status code should be 204

    Scenario: Uncomplete tasks allowed method
        When I send a OPTIONS request to "/tasks/0/uncomplete"
        Then the header "Access-Control-Allow-Methods" should be equal to "POST"
        And the header "Access-Control-Allow-Headers" should be equal to "Content-Type, Authorization"
        Then the response status code should be 204

    Scenario: Complete a task
        When I send a POST authenticate request to "/tasks/0/complete" with foo:bar
        Then the response status code should be 200
        And the JSON should be equal to:
        """
        {
            "status": "success",
            "status_code": 200,
            "status_text": "OK",
            "current_content": "",
            "message": "Task 0 updated"
        }
        """

        When I send a GET authenticate request to "/tasks/0" with foo:bar
        Then the response should be in JSON
        And the JSON node "complete" should be equal to "1"
        And the JSON node "completed" should exist

        When I send a POST authenticate request to "/tasks/0/uncomplete" with foo:bar
        Then the response status code should be 200
        And the JSON should be equal to:
        """
        {
            "status": "success",
            "status_code": 200,
            "status_text": "OK",
            "current_content": "",
            "message": "Task 0 updated"
        }
        """

        When I send a GET authenticate request to "/tasks/0" with foo:bar
        Then the response should be in JSON
        And the JSON node "complete" should be equal to "0"
        And the JSON node "completed" should not exist
