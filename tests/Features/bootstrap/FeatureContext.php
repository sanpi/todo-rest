<?php

use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\MinkExtension\Context\RawMinkContext;
use Symfony\Component\HttpFoundation\HeaderBag;

class FeatureContext extends RawMinkContext
{
    public $directories = [];

    /**
     * @AfterScenario
     */
    public function afterStep($event)
    {
        $this->directories[] = 'app/data/foo/';

        foreach ($this->directories as $directory) {
            exec("rm -rf $directory");
        }
    }

    /**
     * @Given I create the directory :directory
     */
    public function createDirectory($directory)
    {
        mkdir($directory);
        $this->directories[] = $directory;
    }

    /**
     * @Then I send a :method authenticate request to :uri with :username::password
     */
    public function authRequest($method, $uri, $username, $password)
    {
        $this->digest($username, $password, $method, $uri);
    }

    /**
     * @When I set digest Authentication with :username and :password
     */
    public function digestAuth($username, $password)
    {
        $request = $this->getSession()
            ->getDriver()
            ->getClient()
            ->getInternalRequest();
        $this->digest($username, $password, $request->getMethod(), $request->getUri(), $request->getParameters());
    }

    private function digest($username, $password, $method, $uri, $parameters = [])
    {
        $url = $this->locatePath($uri);
        $client = $this->getSession()->getDriver()->getClient();
        $client->setServerParameters([]);
        $client->request($method, $url, $parameters);

        $headers = $client->getResponse()->getHeaders();

        if (isset($headers['WWW-Authenticate'][0])) {
            $headers = new HeaderBag();
            $auth = new Sanpi\Http\Auth\Basic();
            $authorization = $auth->getAuthorization($method, $uri, $headers, $username, $password);

            $client->setServerParameter('Authorization', $authorization);
            $client->request($method, $url, $parameters);
        }
        else {
            throw new \RuntimeException("Not authentification require");
        }
    }
}
