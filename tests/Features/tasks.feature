Feature: REST API

    Background:
        Given I create the directory "app/data/foo/"
        And I create the file "app/data/foo/todo.txt" contening:
        """
        (A) Crack the Da Vinci Code.
        (B) +winning Win.
        @context Give it some context.
        Just a POD: Plain old task.
        (C) +project @context This one has it all!
        (C) 2012-02-03 This one has a date!
        (B) 2012-03-04 +project @context This one has a date and a context AND a project!
        x 2012-04-03 This one has no priority and a date.
        """
        And I create the file "app/data/htpasswd" contening:
        """
        {"foo":{"password":"bar"}}
        """
        And I add "Accept" header equal to "application/json"

    Scenario: Tasks list access control
        When I send a GET authenticate request to "/tasks" with foo:bar
        Then the header "Access-Control-Allow-Origin" should be equal to "*"

    Scenario: Tasks allowed method
        When I send a OPTIONS request to "/tasks"
        Then the header "Access-Control-Allow-Methods" should be equal to "GET, OPTIONS, POST"
        And the header "Access-Control-Allow-Headers" should be equal to "Content-Type, Authorization"
        Then the response status code should be 204

    Scenario: Tasks list
        When I send a GET authenticate request to "/tasks" with foo:bar
        Then the response status code should be 200
        And the response should be in JSON
        And the JSON should be equal to:
        """
        [
            {
                "id": 0,
                "raw": "(A) Crack the Da Vinci Code.",
                "contexts": [],
                "projects": [],
                "priority": "A",
                "complete": false,
                "description": "Crack the Da Vinci Code."
            },
            {
                "id": 1,
                "raw": "(B) +winning Win.",
                "contexts": [],
                "projects": [
                    "winning"
                ],
                "priority": "B",
                "complete": false,
                "description": "Win."
            },
            {
                "id": 2,
                "raw": "@context Give it some context.",
                "contexts": [
                    "context"
                ],
                "projects": [],
                "complete": false,
                "description": "Give it some context."
            },
            {
                "id": 3,
                "raw": "Just a POD: Plain old task.",
                "contexts": [],
                "projects": [],
                "complete": false,
                "description": "Just a POD: Plain old task."
            },
            {
                "id": 4,
                "raw": "(C) +project @context This one has it all!",
                "contexts": [
                    "context"
                ],
                "projects": [
                    "project"
                ],
                "priority": "C",
                "complete": false,
                "description": "This one has it all!"
            },
            {
                "id": 5,
                "raw": "(C) 2012-02-03 This one has a date!",
                "created": "2012-02-03",
                "contexts": [],
                "projects": [],
                "priority": "C",
                "complete": false,
                "description": "This one has a date!"
            },
            {
                "id": 6,
                "raw": "(B) 2012-03-04 +project @context This one has a date and a context AND a project!",
                "created": "2012-03-04",
                "contexts": [
                    "context"
                ],
                "projects": [
                    "project"
                ],
                "priority": "B",
                "complete": false,
                "description": "This one has a date and a context AND a project!"
            },
            {
                "id": 7,
                "raw": "x 2012-04-03 This one has no priority and a date.",
                "contexts": [],
                "projects": [],
                "complete": true,
                "completed": "2012-04-03",
                "description": "This one has no priority and a date."
            }
        ]
        """
